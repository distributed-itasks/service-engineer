implementation module serviceengineer

import StdBool
import StdString
import StdInt
import StdTuple
import StdFile
import StdOrdList
from StdFunc import const, o

import System.OS

from iTasks._Framework.Generic import class iTask
from iTasks.API.Core.Types import :: Task, generic gEq, generic gDefault, generic JSONDecode, generic JSONEncode, generic gText, generic gVerify, generic gUpdate, generic gEditMeta, generic gEditor, :: VerifiedValue, :: Verification, :: MaskedValue, :: DataPath, :: InteractionMask, :: TaskAttributes, :: DateTime, instance toString DateTime
from Data.Maybe import :: Maybe
from iTasks.API.Extensions.User import class toUserConstraint(..), :: UserConstraint, instance toString UserConstraint, instance toUserConstraint User, instance toString UserConstraint, instance toString User
from Text.JSON import :: JSONNode, generic JSONEncode, generic JSONDecode
from iTasks._Framework.Generic.Visualization import :: TextFormat(..)
from iTasks._Framework.Generic.Interaction import :: VSt, ::USt, :: VisualizationResult,:: EditMeta, :: VerifyOptions
import qualified iTasks.API.Extensions.User as U
from iTasks.API.Common.TaskCombinators import -&&-, >>-
from iTasks.API.Core.SDSs import currentDateTime
from iTasks.API.Extensions.User import currentUser, :: User(..), :: UserTitle, :: Role, :: UserId, :: SessionId, assign, workerAttributes, :: Password, :: Username, workAs, :: Credentials{..}, users
from iTasks._Framework.SDS import :: ReadWriteShared, :: RWShared, :: ReadOnlyShared
import qualified iTasks.API.Core.Tasks as C
from iTasks.API.Core.Tasks import accWorld
from iTasks.API.Extensions.Distributed.Engine import startDistributedEngine
from iTasks.API.Extensions.Distributed.Instance import instanceServer, instanceClient, instanceFilter, instanceClameFilter
from Data.Void import :: Void
from Data.Map import :: Map
from iTasks.API.Extensions.Admin.WorkflowAdmin import workflow, class toWorkflow(..), :: Workflow, publish, :: PublishedTask{..}, :: TaskWrapper(..), manageWorklist, instance toWorkflow (Task a), instance toWorkflow (WorkflowContainer a), instance toWorkflow (a -> Task b), instance toWorkflow (ParamWorkflowContainer a b), :: WorkflowContainer, :: ParamWorkflowContainer
from iTasks._Framework.Engine import :: ServiceFormat(..), :: WebAppOption, startEngineWithOptions, startEngine, class Publishable, :: ServerOptions{..}, DEFAULT_KEEPALIVE_TIME, instance Publishable [PublishedTask]
from Internet.HTTP import :: HTTPRequest(..), :: HTTPUpload, :: HTTPProtocol, :: HTTPMethod
import iTasks.API.Common.TaskCombinators
from iTasks.API.Core.TaskCombinators import :: TaskCont
from iTasks.API.Common.InteractionTasks import enterInformation, :: EnterOption, :: ViewOption, enterChoice, :: ChoiceOption, viewInformation, enterChoiceWithShared, waitForTimer, updateInformationWithShared, updateSharedInformation
from iTasks.API.Core.Types import class descr, :: Time
from iTasks.API.Extensions.Admin.UserAdmin import manageUsers, loginAndManageWorkList
from iTasks.API.Core.SDSs import currentTime
from iTasks.API.Core.SDSs import sharedStore
from iTasks.API.Extensions.Distributed.Task import :: Domain
import iTasks.API.Extensions.Distributed.Task
import iTasks.API.Extensions.Distributed.SDS
from iTasks.API.Extensions.Distributed.Authentication import domainAuthServer, usersOf, remoteAuthenticateUser, startAuthEngine, enterDomain, currentDistributedUser, currentDomain
import libaryutil
from iTasks.UI.Layout import :: NoUserInterface(..), instance tune NoUserInterface
from iTasks.API.Extensions.Device.Features import hasCamera, device, :: DeviceFeatures, manageDeviceFeaturs
from iTasks.API.Extensions.Picture.JPEG import :: JPEGPicture(..)
from iTasks.API.Extensions.Device.Camera import takePicture
from iTasks.API.Extensions.Device.Location import :: Coordinates(..), getLocation
import Text
from iTasks.API.Extensions.SVG.SVGlet import overlay, empty, rect, imageUpdate, :: Image, :: Host, :: ImageOffset(..), :: ImageAlign(..), :: UpdateOption, :: Span, :: UpdateOption, :: Conflict, :: TagSource, :: YAlign, :: XAlign, :: TagRef, :: ImageTag
import Graphics.Scalable
import iTasks.API.Extensions.Distributed.InteractionTasks

from Internet.HTTP import :: HTTPResponse{..}, :: HTTPMethod(..)
from Text.URI import :: URI{..}, parseURI
import qualified Text as T 
from iTasks.API.Core.IntegrationTasks import callHTTP

from graph_to_sapl_string import graph_to_sapl_string 

from GoogleMap import :: GoogleMap{..}, :: GoogleMapSettings{..}, :: GoogleMapPosition{..}, :: GoogleMapPerspective{..}, :: GoogleMapMarker{..}, :: GoogleMapIcon(..), :: GoogleMapComplexIcon{..}, :: GoogleMapType(..)

myTasks :: Bool -> [Workflow]
myTasks domainServer =
	[] ++ (if (domainServer) domainServerWork serverWork) 
where
	domainServerWork = 
		[ workflow ("Manage users") 		"Manage system users..." manageUsers
		, workflow "Auth server"                "Auth server" (domainAuthServer) 
		, workflow "Task pool server"   	"Task pool server"      hostTaskPoolServer
		, workflow "Create ticket"		"Create ticket"		createTicket
		, workflow "engineer status"		"Engineer status"	engineerStatus
		]

	serverWork =
		[ workflow "Intermediate task pool"	"Intermediate task pool" intermediateTaskPoolServer 
		, workflow "Task pool client"   	"Task pool client" 	 connectToTaskPoolServer
		]

:: ConnectToTaskPool = { domain :: Domain, port :: Int }

derive class iTask ConnectToTaskPool

hostTaskPoolServer :: Task Void
hostTaskPoolServer
	= getDomain
	>>- \domain -> enterInformation "Task pool port" []
	>>= \port -> (instanceServer port domain -|| instanceFilter (const True) domain)

connectToTaskPoolServer :: Task Void
connectToTaskPoolServer
	= enterInformation "Connect to task pool" []
	>>= \{ConnectToTaskPool|domain=(Domain host),port} -> (instanceClient host port (Domain host)) -|| (instanceFilter (const True) (Domain host))

intermediateTaskPoolServer :: Task Void
intermediateTaskPoolServer
	= enterInformation "Enter YOUR subdomain" []
	>>= \subdomain -> enterInformation "Enter a port for YOUR task pool server" []
	>>= \serverPort -> enterInformation "Connect to (master) task pool" []
	>>= \{ConnectToTaskPool|domain=(Domain host),port} -> ((instanceClient host port (Domain host)) -|| (instanceClameFilter (const True) (Domain host))) -|| instanceServer serverPort subdomain

:: Address = { address :: String, zipcode :: String, city :: String }
:: ContactPerson = { name :: String, email :: Maybe String, phone :: Maybe String }
:: Ticket = { address :: Address, contactPerson :: ContactPerson, note :: Maybe Note }

derive class iTask Address, ContactPerson, Ticket

engineerStatus :: Task Void
engineerStatus
	= get currentDomain
	>>- \domain -> usersOf domain
	>>- \users -> enterChoice "Select a user" [] users
	>>= \user -> user @. domain @: locationTask
where
	locationTask :: Task Void
	locationTask
		= 'C'.get currentUser
		>>- \user -> forever (
			waitForTimer interval
			>>| getLocation 
			>>- \loc -> case loc of
					(Just cords) -> upd (\data -> [i \\ i=:(user, loc) <- data | not (user == (toString user))] ++ [(toString user, cords)]) engineerLocation @! Void
					_	     -> return Void
			) @! Void
	
	interval :: Time
	interval = {Time| hour = 0, min = 2, sec = 0 }

createTicket :: Task (Ticket, [Reparation])
createTicket
	= get currentDomain
	>>- \domain -> enterInformation "Create ticket" []
	>>= \ticket=:{Ticket|address} -> selectEngineer address
	>>- \engineer -> engineer @. domain @: (workOnTicket ticket)
	>>- viewInformation "Ticket result" []

workOnTicket :: Ticket -> Task (Ticket, [Reparation])
workOnTicket ticket=:{Ticket|address}
	= viewInformation "Start" [] "Start"
	>>| getCoordinates address
	>>- \coordinates -> getLocation
	>>- \myCoordinates -> (viewInformation "Machine location" [] address)
	||- viewInformation () []
	{ GoogleMap| settings = mapSettings
	, perspective = (mapPerspective coordinates)
	, markers = (mapMarkers coordinates myCoordinates)
	}
	>>* [OnAction (Action "Start reparation" []) (always ((viewInformation "Details" [] ticket) ||- (reparation >>- \result -> return (ticket, result))))]
where
	mapSettings = { GoogleMapSettings| mapTypeControl = False
		      , panControl = True
		      , zoomControl = True
		      , streetViewControl = True
		      , scaleControl = True
		      , scrollwheel = True
		      , draggable = True
		      }
	mapPerspective (LatLon lat lon) 
		= { GoogleMapPerspective| type = ROADMAP
		  , center = {GoogleMapPosition| lat = (toString lat), lng = (toString lon)}
		  , zoom = 13
		  }
	mapMarkers (LatLon lat lon) myCoordinates
		= [ {GoogleMapMarker| markerId = "customer", position = {GoogleMapPosition| lat = (toString lat), lng = (toString lon)}, title = Nothing, icon = Nothing, infoWindow = Nothing, draggable = False, selected = False}
		  ] ++ case myCoordinates of
			(Just (LatLon myLat myLon)) -> [ {GoogleMapMarker| markerId = "me", position = {GoogleMapPosition| lat = (toString myLat), lng = (toString myLon)}, title = Nothing, icon = (Just (GoogleMapSimpleIcon "van.png")), infoWindow = Nothing, draggable = False, selected = False} ]
			_                           -> []

:: Reparation = TextLine Text
              | PartLine Part
              | CostLine Cost

:: Part = { sku :: String, count :: Real, price :: Real }
:: Text = { note :: String, type :: TextType }
:: Cost = { cost :: CostCode, price :: Real }

:: CostCode = Investigation
            | Installation
            | Other OtherCost

:: OtherCost = { description :: String }

:: TextType = VisableForCustomer | Internal

derive class iTask Reparation, Part, Text, TextType, CostCode, OtherCost, Cost

reparation :: Task [Reparation]
reparation
	= enterInformation "Reparation" [] 
	>>* [ OnAction (Action "Complete" []) (ifValue (not o isEmpty) return)
            ]

engineerLocation :: Shared [(String, Coordinates)]
engineerLocation = sharedStore "engineerLocation" []

selectEngineer :: Address -> Task User
selectEngineer address
	= getCoordinates address
	>>- getEngineers
	>>- enterChoice "Select a engineer" []
	>>= \{EngineerChoice|engineer} -> return engineer

:: EngineerChoice = { engineer :: User, distance :: Maybe Real, traveltime :: Maybe Real }

derive class iTask EngineerChoice

getEngineers :: Coordinates -> Task [EngineerChoice]
getEngineers location
	= get currentDomain
	>>- \domain    -> usersOf domain
	>>- \users     -> get engineerLocation
	>>- \locations -> mapTask (getRoute location locations) users
where
	mapTask :: (a -> Task b) [a] -> Task [b] | iTask a & iTask b
	mapTask _ []        = return []
	mapTask func [x:xs]
		=              func x
		>>- \result -> mapTask func xs
		>>- \other  -> return [result:other]

	getRoute :: Coordinates [(String, Coordinates)] User -> Task EngineerChoice
	getRoute location locations user
		= case [ loc \\ (u, loc) <- locations | u == (toString user) ] of
			[loc:_]	-> routeInfo location loc
				   >>- \{Route|distance,traveltime} -> return {EngineerChoice| engineer = user, distance = Just distance, traveltime = Just traveltime }
			_	-> return {EngineerChoice| engineer = user, distance = Nothing, traveltime = Nothing }

getCoordinates :: Address -> Task Coordinates
getCoordinates address
	= case (url address) of
		(Just uri) -> callHTTP HTTP_GET uri "" parseFun
		_          -> return (LatLon 0.0 0.0)
where
	url :: Address -> Maybe URI
	url {Address|address,zipcode,city}
		= parseURI ('T'.concat ["http://nominatim.openstreetmap.org/search?q="
				       , encode address
				       , "+"
				       , encodeZipcode zipcode
				       , "+"
				       , encode city
				       , "&format=json"
				       ])

	encode text = 'T'.replaceSubString " " "+" ('T'.trim text)

	// 1234 AB is not allowed, use 1234AB
	encodeZipcode zipcode = 'T'.replaceSubString " " "" zipcode

	parseFun :: HTTPResponse -> (MaybeErrorString Coordinates)
	parseFun response
		# data = toJSONResponse response "[" "]"
		# lat =  case jsonQuery "0/lat" data of
				(Just val) 	-> val
				_		-> "0.0"
		# lon = case jsonQuery "0/lon" data of
				(Just val)	-> val
				_		-> "0.0"
		= (Ok (LatLon (toReal lat) (toReal lon)))

round :: Real Int -> Real
round x decimals = (toReal (toInt (x * (toReal (10 ^ decimals))))) / 100.0

:: Route = { distance :: Real, traveltime :: Real }

derive class iTask Route

routeInfo :: Coordinates Coordinates -> Task Route
routeInfo fromLocation toLocation
	= case (url fromLocation toLocation) of
		(Just uri) -> callHTTP HTTP_GET uri "" parseFun
		_	   -> return {Route| distance = 0.0, traveltime = 0.0 }
where
	url :: Coordinates Coordinates -> Maybe URI
	url (LatLon flat flon) (LatLon tlat tlon)
		 = parseURI ('T'.concat [ "http://www.yournavigation.org/api/1.0/gosmore.php?format=geojson&v=motorcar&fast=1&layer=mapnik&geometry=0"
					, "&flat=", (toString flat)
					, "&flon=", (toString flon)
					, "&tlat=", (toString tlat)
					, "&tlon=", (toString tlon)
					])

	parseFun :: HTTPResponse -> (MaybeErrorString Route)
	parseFun response
		# json = toJSONResponse response "{" "}"
		# distance = case jsonQuery "properties/distance" json of
				(Just val) 	-> val
				_		-> "0.0"
		# traveltime = case jsonQuery "properties/traveltime" json of
				(Just val)	-> val
				_		-> "0"
		= (Ok {Route| distance = round (toReal distance) 2, traveltime = (round ((toReal traveltime) / 60.0) 2)})

toJSONResponse :: HTTPResponse String String -> JSONNode
toJSONResponse {HTTPResponse| rsp_data} start end
	# s = 'T'.indexOf start rsp_data
	# e = 'T'.lastIndexOf end rsp_data
	| s == -1 || e == -1 = JSONNull
	# rsp_data = subString s (e - (s - 1)) rsp_data
	= fromString rsp_data

Start :: *World -> *World
Start world
	= startEngine 	[ publish "/" (WebApp []) (\_-> startMode (IF_WINDOWS "serviceengineer.exe" "serviceengineer"))
			] world

:: ServerRole = DomainServer Domain
	      | Server Domain
	      | NoneServer

derive class iTask ServerRole

serverRoleShare :: Shared ServerRole
serverRoleShare = sharedStore "serverRoleShare" NoneServer

getDomain :: Task Domain
getDomain
	= get serverRoleShare
	>>- \info -> case info of
			(DomainServer domain) -> return domain
			(Server domain)	-> return domain

startMode :: String -> Task Void
startMode executable
	= startDistributedEngine executable
	>>| get serverRoleShare 
	>>- \role = case role of
			DomainServer domain -> startAuthEngine domain >>| loginAndManageWorkList "Service engineer application" (myTasks True)
			Server domain -> startAuthEngine domain >>| loginRemote (myTasks False)
			_ -> viewInformation "Welcome" [] "Chose what this iTasks instance is."
		             >>* [ OnAction (Action "Domain server" []) (always (domainServer))
            			 , OnAction (Action "Server" []) (always (server))
            			 ]
where
	server :: Task Void
	server
		= enterDomain 
		>>= \domain -> set (Server domain) serverRoleShare
		>>| startAuthEngine domain >>| loginRemote (myTasks False)

	domainServer :: Task Void
	domainServer
		= enterDomain
		>>= \domain -> set (DomainServer domain) serverRoleShare
		>>| startAuthEngine domain
		>>| loginAndManageWorkList "Service engineer application" (myTasks True)

loginRemote :: ![Workflow] -> Task Void
loginRemote workflows 
	= forever (
		enterInformation "Enter your credentials and login" []
		>>* 	[OnAction (Action "Login" [ActionIcon "login",ActionKey (unmodified KEY_ENTER)]) (hasValue (browseAuthenticated workflows))
				]
	)
where
	browseAuthenticated workflows {Credentials|username,password}
		= remoteAuthenticateUser username password
		>>= \mbUser -> case mbUser of
			Just user 	= workAs user (manageWorklist workflows)
			Nothing		= viewInformation (Title "Login failed") [] "Your username or password is incorrect" >>| return Void

StartAndroid :: !String !String !String !String !String -> Int
StartAndroid appName appPath sdkPath storeOpt libPath
	= if (libaryStart start) 42 0
where
	start :: *World -> *World
	start world
		# options =
			{ appName               = appName
			, appPath               = appPath
			, sdkPath               = if (sdkPath == "") Nothing (Just sdkPath)
			, serverPort            = 8080
			, keepalive             = DEFAULT_KEEPALIVE_TIME
			, webDirPaths           = Nothing
			, storeOpt              = Just storeOpt
			, saplOpt               = Nothing
			}
		= startEngineWithOptions [ publish "/" (WebApp []) (\_-> startMode libPath)
					 ] options world

// Needed for Android to call Start.
foreign export StartAndroid
