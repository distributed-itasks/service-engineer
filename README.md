# Service engineer

Service engineer iTasks application

## Build

Clone the [cross compiler](https://gitlab.science.ru.nl/distributed-itasks/cross-compiler) repository.

For the targets Linux (32- and 64-bits), Raspberry Pi, Windows 32-bit and Android you can use the Docker image. Build the docker image: `sudo docker build -t "cleanitasks:cross-compilers" <path/to/cross-compiler>`
When the Docker image is build you can build the iTasks application using:

```
docker run -v <path/to/this/folder>:/usr/src/app -v <path/to/itasks-android>:/usr/src/android-app cleanitasks:cross-compilers cpm <target> serviceengineer.<target>.prj
```

The targets are:

* **linux32**
* **linux64**
* **raspberrypi**
* **win32**
* **android**, copy the `.apk` file in the `android` folder to your Android device running Android 5.1 or later and open the `.apk` file using a file browser application.

When building for the target MacOS see the instructions in the README.md file of the cross compiler repsoitory.
