implementation module libaryutil

import System._Unsafe

libaryStart :: !(*World -> *World) -> Bool
libaryStart start = appUnsafe start True
