definition module libaryutil

/*
 * When c calls a Clean function you won’t have a world. 
 * This function fixes this problem by creating one for you. Note that creating a world is unsafe.
 */
libaryStart :: !(*World -> *World) -> Bool
